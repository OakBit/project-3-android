package boflo.app.android.project3;

import android.location.Address;
import android.location.Geocoder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class AddActivity extends AppCompatActivity {

    EditText et;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        et = (EditText)findViewById(R.id.editText);
    }

    /*
* Geocodes a latlng value
*/
    public Address getLocationFromAddress(String address){
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        Address retval = new Address(new Locale("en","ca"));
        String errorMessage = "";

        List<Address> addresses = null;

        try {
            addresses = geocoder.getFromLocationName(   address, 1);
        } catch (IOException ioException) {
            // Catch network or other I/O problems.
            errorMessage = getString(R.string.service_not_available);
            Log.e("getAddress", errorMessage, ioException);
        } catch (IllegalArgumentException illegalArgumentException) {
            // Catch invalid latitude or longitude values.
            errorMessage = getString(R.string.invalid_lat_long_used);
        }
        if(addresses.size() > 0 ){
            return addresses.get(0);
        }else {
            return new Address(new Locale("en","ca"));
        }
    }

    public void add_clicked(View view) {
        Address addr = getLocationFromAddress(et.getText().toString());
        String addressStr = "";
        if(addr.getThoroughfare() != null)
            addressStr += addr.getThoroughfare();
        addressStr = "" + addr.getLocality() + " " + addr.getAdminArea();
        DrawerActivity.locations.add(new LocationDetails(addressStr, new LatLng(addr.getLatitude(), addr.getLongitude()),new Date()));
        Toast.makeText(this, "Added location", Toast.LENGTH_SHORT).show();
        finish();
    }
}
