package boflo.app.android.project3;

import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.DirectionsApi;
import com.google.maps.DirectionsApiRequest;
import com.google.maps.GeoApiContext;
import com.google.maps.model.DirectionsLeg;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.DirectionsRoute;
import com.google.maps.model.DirectionsStep;
import com.google.maps.model.EncodedPolyline;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {
/*
 * Change markers to be a pin
 *
 */
    private GoogleMap mMap;
    private int zoomlevel = 9;
    private int lineColor = Color.BLUE;

    public static ArrayList<LocationDetails> paths = new ArrayList<LocationDetails>();

    public class Path{
        double sourceLat;
        double sourceLng;
        double destLat;
        double destLng;

        public LatLng getSourceLatLng(){
            return new LatLng(sourceLat, sourceLng);
        }
        public void setSourceLatLng(LatLng loc){
            sourceLat = loc.latitude;
            sourceLng = loc.longitude;
        }
        public LatLng getDestLatLng(){
            return new LatLng(destLat, destLng);
        }
        public void setDestLatLng(LatLng loc){
            destLat = loc.latitude;
            destLng = loc.longitude;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng pos = new LatLng(0d,0d);
        for (int i = 0 ; i < DrawerActivity.locations.size(); ++i) {
            pos = DrawerActivity.locations.get(i).getLatLng();
            mMap.addMarker(new MarkerOptions().position(pos).title(DrawerActivity.locations.get(i).toString()).icon(BitmapDescriptorFactory.fromResource(R.drawable.push_pin))).setAnchor(0.5f,0.5f);
            mMap.moveCamera(CameraUpdateFactory.newLatLng(pos));

            /*
            * Draw all points on the map and connect them with polylines,
            * New days have different colour lines
            * The older the day the more faded it becomes?
            * Can disable lines and markers for certain days
            * Only show last 2 days
            * */

            int prev = i-1;
            if(prev >-1) {
                //Save the path somehow so we dont have to reload every time
                List<LatLng> path = getPathBetweenPointsAlongRoad(DrawerActivity.locations.get(prev).getLatLng(), DrawerActivity.locations.get(i).getLatLng());
                //Draw the polyline
                if (path.size() > 0) {

                    PolylineOptions opts = new PolylineOptions().addAll(path).color(lineColor).width(10);
                    mMap.addPolyline(opts);

                }
            }
        }
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(pos, zoomlevel));

    }


    public List<LatLng> getPathBetweenPointsAlongRoad(LatLng source, LatLng dest){
        //Define list to get all latlng for the route
        List<LatLng> path = new ArrayList();


        //Execute Directions API request
        GeoApiContext context = new GeoApiContext.Builder()
                .apiKey("AIzaSyAi4rQFtqXytlvYMueF1FF2mLT73P5Wfdo")//different than google maps key, this is for web api 2500 uses per day
                .build();

        //Inefficient use, should send multiple points at once to api
        DirectionsApiRequest req = DirectionsApi.getDirections(context, source.latitude + "," + source.longitude, dest.latitude + "," + dest.longitude);

        try {
            DirectionsResult res = req.await();

            //Loop through legs and steps to get encoded polylines of each step
            if (res.routes != null && res.routes.length > 0) {
                DirectionsRoute route = res.routes[0];

                if (route.legs !=null) {
                    for(int i=0; i<route.legs.length; i++) {
                        DirectionsLeg leg = route.legs[i];
                        if (leg.steps != null) {
                            for (int j=0; j<leg.steps.length;j++){
                                DirectionsStep step = leg.steps[j];
                                if (step.steps != null && step.steps.length >0) {
                                    for (int k=0; k<step.steps.length;k++){
                                        DirectionsStep step1 = step.steps[k];
                                        EncodedPolyline points1 = step1.polyline;
                                        if (points1 != null) {
                                            //Decode polyline and add points to list of route coordinates
                                            List<com.google.maps.model.LatLng> coords1 = points1.decodePath();
                                            for (com.google.maps.model.LatLng coord1 : coords1) {
                                                path.add(new LatLng(coord1.lat, coord1.lng));
                                            }
                                        }
                                    }
                                } else {
                                    EncodedPolyline points = step.polyline;
                                    if (points != null) {
                                        //Decode polyline and add points to list of route coordinates
                                        List<com.google.maps.model.LatLng> coords = points.decodePath();
                                        for (com.google.maps.model.LatLng coord : coords) {
                                            path.add(new LatLng(coord.lat, coord.lng));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch(Exception ex) {
            Log.e("locationList", ex.getLocalizedMessage());
        }

        return path;
    }

}
