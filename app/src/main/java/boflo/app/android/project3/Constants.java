/*
 * Copyright 2015 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package boflo.app.android.project3;

public class Constants {

    private Constants() {};

    public static final int SUCCESS_RESULT = 0;
    public static final int FAILURE_RESULT = 1;


    public static final String PACKAGE_NAME = "boflo.app.android.project3";
    //Shared pref id's
    public static final String SHARED_PREF_NAME = "boflo.app.android.project3.preferences";
    public static final String PREF_LOCATIONS = SHARED_PREF_NAME + "locations_collection";
    public static final String PREF_LOCATION_PATHS = SHARED_PREF_NAME + "location_paths";

    // Maps values
    public static final String MAPS_INTENT_URI = "geo:0,0?q=";
    public static final String MAPS_NAVIGATION_INTENT_URI = "google.navigation:mode=w&q=";

}
