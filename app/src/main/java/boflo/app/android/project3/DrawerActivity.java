package boflo.app.android.project3;

import android.*;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class DrawerActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks, AdapterView.OnItemClickListener {

    @Override
    protected void onStart() {
        if (googleApiClient != null) {
            googleApiClient.connect();
        }
        super.onStart();


    }

    @Override
    protected void onStop() {
        googleApiClient.disconnect();
        super.onStop();

        SharedPreferences prefs = getSharedPreferences(Constants.SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        try {
            String s = ObjectSerializer.serialize(locations);
            editor.putString(Constants.PREF_LOCATIONS, ObjectSerializer.serialize(locations));
        } catch (IOException e) {
            e.printStackTrace();
        }
        editor.commit();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    public static ArrayList<LocationDetails> locations = new ArrayList<LocationDetails>();

    ListView lv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
////                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
////                        .setAction("Action", null).show();
//            }
//        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        lv = (ListView) findViewById(R.id.listView);

        /*
        REQUEST PERMISSIONS
         */
        // Check fine location permission has been granted
        if (!Utils.checkFineLocationPermission(this)) {
            // See if user has denied permission in the past
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this, android.Manifest.permission.ACCESS_FINE_LOCATION)) {
                // Show a simple snackbar explaining the request instead
            } else {
                // Otherwise request permission from user
                if (savedInstanceState == null) {
                    requestFineLocationPermission();
                }
            }
        } else {
            // Otherwise permission is granted (which is always the case on pre-M devices
        }
        //Reload our locations
        SharedPreferences prefs = getSharedPreferences(Constants.SHARED_PREF_NAME, Context.MODE_PRIVATE);

        try {
            locations = (ArrayList<LocationDetails>) ObjectSerializer.deserialize(prefs.getString(Constants.PREF_LOCATIONS, ObjectSerializer.serialize(new ArrayList<LocationDetails>())));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (null == locations) {
            locations = new ArrayList<LocationDetails>();
        }
        final ArrayAdapter<LocationDetails> arrayAdapter = new ArrayAdapter<LocationDetails>
                (this, android.R.layout.simple_list_item_1, locations);

        lv.setAdapter(arrayAdapter);
        lv.setOnItemClickListener(this);

        /*TEST VALUES*/
        //locations.add(new LocationDetails("ADDRESS", new LatLng(42.9849d,-81.2453d),new Date()));
        //locations.add(new LocationDetails("ADDRESS", new LatLng(43.6532d,-79.3832d),new Date()));

        googleApiClient = new GoogleApiClient.Builder (this, this, this).addApi(LocationServices.API).build();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //(R.menu.activity_drawer_drawer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_about) {
            // about activity
            Intent intent = new Intent(this,AboutActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_send) {
            String [] to = {};//user decides where to send
            String [] cc = {};//user decides where to send
            Intent emailIntent = new Intent(Intent.ACTION_SEND);
            emailIntent.setData(Uri.parse("mailto:"));
            emailIntent.putExtra(Intent.EXTRA_EMAIL, to);
            emailIntent.putExtra(Intent.EXTRA_CC, cc);
            String content = "Places visited : ";
            for (LocationDetails det:DrawerActivity.locations) {
                content += "\n" + det.address + " on " + det.dateRecorded;
            }
            emailIntent.putExtra(Intent.EXTRA_TEXT,"Hello, this is record from 'Traveller' mobile app \n" + content + "\n\nWant to know more about this? Visit www.boandflo.com");
            emailIntent.putExtra(Intent.EXTRA_SUBJECT,"Traveller app - Records");
            emailIntent.setType("message/rfc822");
            startActivity(Intent.createChooser(emailIntent,"Email"));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void onClickMap(View view) {
        Toast.makeText(this, "Loading Map", Toast.LENGTH_SHORT).show();
        //check the map
        Intent intent = new Intent(this,MapsActivity.class);
        startActivity(intent);
    }

    private static final int PERMISSION_REQ = 0;

    private GoogleApiClient googleApiClient;


    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    /**
     * Permissions request result callback
     */
    @Override
    public void onRequestPermissionsResult(
            int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQ:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Run when permissions granted
                }
        }
    }

    /**
     * Request the fine location permission from the user
     */
    private void requestFineLocationPermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQ);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        ((ArrayAdapter<LocationDetails>)lv.getAdapter()).notifyDataSetChanged();
    }

    public void checkLocation_clicked(View view){
        LatLng tmp = getLocation();
        Address tmp1 = getAddress(tmp);
        String addr = "" + tmp1.getThoroughfare() + " " + tmp1.getLocality() + " " + tmp1.getAdminArea();
        locations.add(new LocationDetails(addr,tmp, new Date()));
        ((ArrayAdapter<LocationDetails>)lv.getAdapter()).notifyDataSetChanged();
    }

    public void checkMap_clicked(View view) {
        Intent intent = new Intent(this,MapsActivity.class);
        startActivity(intent);
    }

    public void clearList_clicked(View view) {
        locations.clear();
        ((ArrayAdapter<LocationDetails>)lv.getAdapter()).notifyDataSetChanged();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        LocationDetails details = (LocationDetails)lv.getItemAtPosition(i);

    }

    /*
     * Retrieves last location as latlng
     */
    public LatLng getLocation() {
        if (!Utils.checkFineLocationPermission(this)) {
            requestFineLocationPermission();
        }
        Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        return new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude());
    }

    /*
     * Geocodes a latlng value
     */
    public Address getAddress(LatLng location){
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        Address retval = new Address(new Locale("en","ca"));
        String errorMessage = "";

        List<Address> addresses = null;

        try {
            addresses = geocoder.getFromLocation(
                    location.latitude,
                    location.longitude,
                    // In this sample, get just a single address.
                    1);
        } catch (IOException ioException) {
            // Catch network or other I/O problems.
            errorMessage = getString(R.string.service_not_available);
            Log.e("getAddress", errorMessage, ioException);
        } catch (IllegalArgumentException illegalArgumentException) {
            // Catch invalid latitude or longitude values.
            errorMessage = getString(R.string.invalid_lat_long_used);
            Log.e("getAddress", errorMessage + ". " +
                    "Latitude = " + location.latitude +
                    ", Longitude = " +
                    location.longitude, illegalArgumentException);
        }

        // Handle case where no address was found.
        if (addresses == null || addresses.size()  == 0) {
            if (errorMessage.isEmpty()) {
                errorMessage = getString(R.string.no_address_found);
                Log.e("getAddress", errorMessage);
            }
        } else {
            Address address = addresses.get(0);
            ArrayList<String> addressFragments = new ArrayList<String>();

            // Fetch the address lines using getAddressLine,
            // join them, and send them to the thread.
            for(int i = 0; i <= address.getMaxAddressLineIndex(); i++) {
                addressFragments.add(address.getAddressLine(i));
            }
            Log.i("getAddress", getString(R.string.address_found));
            retval = address;
        }
        return retval;
    }



    public void onClickAdd(MenuItem item) {
        // about activity
        Intent intent = new Intent(this,AddActivity.class);
        startActivity(intent);
    }
}
